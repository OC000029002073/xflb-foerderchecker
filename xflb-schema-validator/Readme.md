# Förderchecker

Der "Förderchecker" bietet einen schnellen und sicheren Weg XML-Dateien gegen bestimmten XSD-Format 
zu überprüfen.

## Beschreibung

Der "Förderchecker" definiert einen IXmlValidator Interface. Die Standard-Implementierung verwendet einen
einfachen XmlSchemaResolver, bindet die javax.xml.parsers.SAXParserFactory Klasse für die Analyse der XML-Datei an 
und liefert einen Validierungsergebnis mit zusätzlichen Fehlerinformationen.