<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xflb="http://xoev.de/schemata/xflb/1_0_3" targetNamespace="http://xoev.de/schemata/xflb/1_0_3" elementFormDefault="qualified" attributeFormDefault="unqualified" version="1.0.3">
	<xs:annotation>
		<xs:appinfo>
			<standard>
				<nameLang>XFLB - XÖV-Standard für Förderleistungsbeschreibungen</nameLang>
				<nameKurz>XFLB</nameKurz>
				<nameTechnisch>xflb</nameTechnisch>
				<kennung>urn:xoev-de:foefi:standard:xflb</kennung>
				<beschreibung>XFLB standardisiert den von Produkt und Hersteller unabhängigen Austausch von Informationen zu Förderleistungen.</beschreibung>
			</standard>
			<versionStandard>
				<version>1.0.3</version>
				<versionXOEVProfil/>
				<versionXOEVHandbuch/>
				<versionXGenerator/>
				<versionModellierungswerkzeug/>
				<nameModellierungswerkzeug/>
			</versionStandard>
		</xs:appinfo>
		<xs:documentation>Das Paket "Fördergebiet" definiert Typen für die Erfassung von Angaben zum Fördergebiet.</xs:documentation>
	</xs:annotation>
	<xs:include schemaLocation="xflb-basisdatentypen.xsd"/>
	<xs:complexType name="OptionFoerdergebietEU">
		<xs:annotation>
			<xs:documentation>Typ zur Repräsentation des Fördergebiets im Fall EU-weit angebotener Förderleistungen</xs:documentation>
		</xs:annotation>
		<xs:simpleContent>
			<xs:restriction base="xflb:OptionEinfach">
				<xs:attribute name="optionCode" type="xs:token" use="required" fixed="001"/>
				<xs:attribute name="listURI" type="xs:anyURI" use="required" fixed="urn:xoev-de:foefi:codeliste:xflb.geldgebendeinstitution_1"/>
				<xs:attribute name="listVersionID" type="xs:normalizedString" use="required" fixed="1"/>
			</xs:restriction>
		</xs:simpleContent>
	</xs:complexType>
	<xs:complexType name="OptionFoerdergebietBund">
		<xs:annotation>
			<xs:documentation>Typ zur Repräsentation des Fördergebiets im Fall bundesweit angebotener Förderleistungen</xs:documentation>
		</xs:annotation>
		<xs:simpleContent>
			<xs:restriction base="xflb:OptionEinfach">
				<xs:attribute name="optionCode" type="xs:token" use="required" fixed="002"/>
				<xs:attribute name="listURI" type="xs:anyURI" use="required" fixed="urn:xoev-de:foefi:codeliste:xflb.geldgebendeinstitution_1"/>
				<xs:attribute name="listVersionID" type="xs:normalizedString" use="required" fixed="1"/>
			</xs:restriction>
		</xs:simpleContent>
	</xs:complexType>
	<xs:complexType name="OptionBundesland">
		<xs:annotation>
			<xs:documentation>Typ zur Repräsentation einzelner Bundesländer im Fall von Förderleistungen, die auf einzelne Bundesländer beschränkt sind</xs:documentation>
		</xs:annotation>
		<xs:simpleContent>
			<xs:restriction base="xflb:OptionEinfach">
				<xs:attribute name="optionCode" use="required">
					<xs:simpleType>
						<xs:restriction base="xs:token">
							<xs:pattern value="0(0[1-9]|1[0-6])"/>
						</xs:restriction>
					</xs:simpleType>
				</xs:attribute>
				<xs:attribute name="listURI" type="xs:anyURI" use="required" fixed="urn:xoev-de:foefi:codeliste:xflb.bundesland_1"/>
				<xs:attribute name="listVersionID" type="xs:normalizedString" use="required" fixed="1"/>
			</xs:restriction>
		</xs:simpleContent>
	</xs:complexType>
	<xs:complexType name="Laenderauswahl">
		<xs:annotation>
			<xs:documentation>Datenstruktur zur Erfassung einer Auswahl von Bundesländern</xs:documentation>
		</xs:annotation>
		<xs:choice>
			<xs:element name="value" type="xflb:OptionBundesland" maxOccurs="unbounded"/>
		</xs:choice>
	</xs:complexType>
	<xs:complexType name="OptionFoerdergebietLand">
		<xs:annotation>
			<xs:documentation>Typ zur Repräsentation des Fördergebiets im Fall von Förderleistungen, die auf einzelne Bundesländer beschränkt sind</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:restriction base="xflb:OptionKomplex">
				<xs:all>
					<xs:element name="betroffeneLaender" type="xflb:Laenderauswahl">
						<xs:annotation>
							<xs:appinfo>
								<Beschriftung>Betroffene Länder</Beschriftung>
								<Ausfuellhinweise/>
							</xs:appinfo>
							<xs:documentation>Auflistung der betroffenen Bundesländer</xs:documentation>
						</xs:annotation>
						<xs:unique name="OptionBundesland_unique">
							<xs:selector xpath="./*"/>
							<xs:field xpath="@optionCode"/>
						</xs:unique>
					</xs:element>
				</xs:all>
				<xs:attribute name="optionCode" type="xs:token" use="required" fixed="003"/>
				<xs:attribute name="listURI" type="xs:anyURI" use="required" fixed="urn:xoev-de:foefi:codeliste:xflb.geldgebendeinstitution_1"/>
				<xs:attribute name="listVersionID" type="xs:normalizedString" use="required" fixed="1"/>
			</xs:restriction>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="OptionFoerdergebietKommune">
		<xs:annotation>
			<xs:documentation>Typ zur Repräsentation des Fördergebiets im Fall von Förderleistungen, die auf einzelne Kommunen beschränkt sind; ermöglicht außerdem die Erfassung der betreffende(n) Kommune(n)</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:restriction base="xflb:OptionKomplex">
				<xs:all>
					<xs:element name="bezeichnung" type="xflb:Bezeichnung">
						<xs:annotation>
							<xs:appinfo>
								<Beschriftung>Bezeichnung</Beschriftung>
								<Ausfuellhinweise/>
							</xs:appinfo>
							<xs:documentation>Angabe einer Bezeichnung für die betreffende(n) Kommune(n)</xs:documentation>
						</xs:annotation>
					</xs:element>
				</xs:all>
				<xs:attribute name="optionCode" type="xs:token" use="required" fixed="004"/>
				<xs:attribute name="listURI" type="xs:anyURI" use="required" fixed="urn:xoev-de:foefi:codeliste:xflb.geldgebendeinstitution_1"/>
				<xs:attribute name="listVersionID" type="xs:normalizedString" use="required" fixed="1"/>
			</xs:restriction>
		</xs:complexContent>
	</xs:complexType>
	<xs:complexType name="Foerdergebiet">
		<xs:annotation>
			<xs:documentation>Datenstruktur zur Aufnahme von Angaben zum Fördergebiet</xs:documentation>
		</xs:annotation>
		<xs:choice>
			<xs:element name="eu" type="xflb:OptionFoerdergebietEU">
				<xs:annotation>
					<xs:documentation>Es handelt sich um eine EU-weit angebotene Förderleistung</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="bund" type="xflb:OptionFoerdergebietBund">
				<xs:annotation>
					<xs:documentation>Es handelt sich um eine bundesweit angebotene Förderleistung</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="land" type="xflb:OptionFoerdergebietLand">
				<xs:annotation>
					<xs:documentation>Es handelt sich um eine auf einzelne Bundesländer beschränkte Förderleistung</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="kommune" type="xflb:OptionFoerdergebietKommune">
				<xs:annotation>
					<xs:documentation>Es handelt sich um eine auf einzelne Kommunen beschränkte Förderleistung</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:choice>
	</xs:complexType>
</xs:schema>
