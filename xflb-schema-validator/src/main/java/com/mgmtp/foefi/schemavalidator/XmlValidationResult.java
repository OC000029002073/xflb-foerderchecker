package com.mgmtp.foefi.schemavalidator;

import java.util.ArrayList;
import java.util.List;

/**
 * Represent the result of an XMLValidation
 */
public class XmlValidationResult {
	private final List<String> xmlErrors = new ArrayList<>();

	/**
	 * Add an XMLError
	 * @param anXmlError String an XMlError
	 */
	public void addXmlError(String anXmlError) {
		xmlErrors.add(anXmlError);
	}

	/**
	 * Check if the result is ok
	 * @return boolean
	 */
	public Boolean isOK() {
		return xmlErrors == null || xmlErrors.isEmpty();
	}

	/**
	 * get the collected XML-Errors
	 * @return List<String> the errors
	 */
	public List<String> getXmlErrors() {
		return xmlErrors;
	}
}
