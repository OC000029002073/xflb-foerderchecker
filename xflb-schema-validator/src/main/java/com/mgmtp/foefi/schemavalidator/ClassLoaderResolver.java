package com.mgmtp.foefi.schemavalidator;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSResourceResolver;

/**
 * Resolve schema using the class loader.
 */
public class ClassLoaderResolver implements LSResourceResolver {

	private String xsdDir;

	public ClassLoaderResolver(String xsdDir) {
		this.xsdDir = xsdDir;
	}

	public String getXsdDir() {
		return xsdDir;
	}

	public void setXsdDir(String xsdDir) {
		this.xsdDir = xsdDir;
	}

	@Override
	public LSInput resolveResource(String type, String namespaceURI, String publicId, String systemId, String baseURI) {
		InputStream resourceAsStream = getResourceAsStream(systemId);
		return new Input(publicId, systemId, resourceAsStream);
	}

	/**
	 * Get resource as Stream, using the ClassLoader
	 * @param systemId String a resource filename
	 * @return InputStream | null if cannot be found.
	 */
	public InputStream getResourceAsStream(String systemId) {
		return getClass()
				.getClassLoader()
				.getResourceAsStream(xsdDir + "/" + systemId );
	}


}
