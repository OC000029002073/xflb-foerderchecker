package com.mgmtp.foefi.schemavalidator;

import java.io.IOException;
import java.io.Reader;

/**
 * Common interface for all validators
 */
public interface IXmlValidator {
	XmlValidationResult validate(Reader xsdReader, Reader xmlReader) throws  IOException;

	/**
	 * Validate an XML Doc, the XSD is loaded from classpath. assert the resourceloader has been loaded properly
	 * @param xmlReader Reader reading the xml doc
	 * @return XflbValidationResult
	 */
	XmlValidationResult validateXflb(Reader xmlReader) throws  IOException;
}
