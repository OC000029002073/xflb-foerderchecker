package com.mgmtp.foefi.schemavalidator;

public class XmSchemaValidationException extends RuntimeException {
	 XmSchemaValidationException(String msg, Throwable e) {
		super(msg, e);
	}
}
