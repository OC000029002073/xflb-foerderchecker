package com.mgmtp.foefi.schemavalidator;

import org.w3c.dom.ls.LSResourceResolver;

public class XmlValidatorFactory {

	/**
	 * @return - new xml validator instance handling xflb
	 */
	public static IXmlValidator getDefaultValidator() {
		LSResourceResolver resolver = new ClassLoaderResolver("xflb/xsd");
		return new XmlValidator(resolver);
	}

	private XmlValidatorFactory(){
		//private constructor
	}
}
