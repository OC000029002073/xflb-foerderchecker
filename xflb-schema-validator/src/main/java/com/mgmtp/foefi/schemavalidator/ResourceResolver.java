package com.mgmtp.foefi.schemavalidator;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSResourceResolver;

/**
 * Resource resolver for loading included schemas
 */
public class ResourceResolver implements LSResourceResolver {

	private String xsdDir;

	public ResourceResolver(String xsdDir) {
		this.xsdDir = xsdDir;
	}

	@Override public LSInput resolveResource(String type, String namespaceURI, String publicId, String systemId, String baseURI) {
		InputStream resourceAsStream = null;
		try {
			resourceAsStream = new FileInputStream(xsdDir + "/" + systemId);
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
		return new Input(publicId, systemId, resourceAsStream);
	}
}
