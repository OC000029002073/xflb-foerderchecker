package com.mgmtp.foefi.schemavalidator;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.w3c.dom.ls.LSResourceResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Validator performing XSD-Validations.
 */
public class XmlValidator implements IXmlValidator {
	private final SchemaFactory schemaFactory =  SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

	public static final String XFLB_MAIN_XSD_FILE = "xflb/xsd/xflb.xsd";

	/**
	 * Inject a schema resolver
	 * @param resolver LSResourceResolver loading subschemas from file
	 */
	public XmlValidator(LSResourceResolver resolver) {
		//Schema resolver for loading external dependencies
		schemaFactory.setResourceResolver(resolver);
	}

	/**
	 * the constructor
	 */
	public XmlValidator() {
	}

	/**
	 * Validate an XML Doc
	 * @param xsdReader - Reader reading the mainxsd
	 * @param xmlReader - Reader reading the xml doc
	 * @return XflbValidationResult
	 */

	public XmlValidationResult validate( Reader xsdReader, Reader xmlReader) throws IOException {
		final SAXParserFactory SAX_PARSER_FACTORY = SAXParserFactory.newInstance();
		try {
			SAX_PARSER_FACTORY.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
			SAX_PARSER_FACTORY.setFeature("http://xml.org/sax/features/external-general-entities", false);
			SAX_PARSER_FACTORY.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
			SAX_PARSER_FACTORY.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			SAX_PARSER_FACTORY.setXIncludeAware(false);
		} catch (SAXNotSupportedException | SAXNotRecognizedException | ParserConfigurationException ex) {
			throw new XmSchemaValidationException("SAXParserFactory implementation does not support secure processing", ex);
		}
		SAX_PARSER_FACTORY.setValidating(false);
		SAX_PARSER_FACTORY.setNamespaceAware(true);

		//Now the parser itself
		XmlValidationResult result = new XmlValidationResult();
		//Creating an instance of our special handler
		final DefaultHandler handler = new SaxParseHandler(result);
		try {
			Source schemaFile = new StreamSource(xsdReader);
			Schema schema =schemaFactory.newSchema(schemaFile);
			//Setting the Schema for validation
			SAX_PARSER_FACTORY.setSchema(schema);
			final SAXParser parser = SAX_PARSER_FACTORY.newSAXParser();
			parser.parse(new InputSource(xmlReader), handler);
		} catch (ParserConfigurationException | SAXException e ) {
			result.addXmlError(e.getClass().toString() + "/" + e.getMessage());
		}
		return result;
	}


	@Override
	public XmlValidationResult validateXflb(Reader xmlReader) throws  IOException {
		Reader xsdReader = getXflbXsdReader();
		return validate(xsdReader,xmlReader);
	}

	private Reader getXflbXsdReader() {
		return new InputStreamReader(
				readFromFile(XFLB_MAIN_XSD_FILE),
				StandardCharsets.UTF_8);
	}

	private InputStream readFromFile(String filepath) {
		return Thread
				.currentThread()
				.getContextClassLoader()
				.getResourceAsStream(filepath);
	}
}