package com.mgmtp.foefi.schemavalidator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * More detailed parse handler, outputing the Elements that are damaged
 */
public class SaxParseHandler extends DefaultHandler {
	private XmlValidationResult validationResult;

    private String element = "";
    private  boolean errorOccurred = false;

    private static final Logger LOG = LoggerFactory.getLogger(SaxParseHandler.class);

	public SaxParseHandler(XmlValidationResult validationResult) {
		super();
		this.validationResult = validationResult;
	}

	@Override
    public void startElement(String uri, String localName, String qName,
                             Attributes attributes) throws SAXException {

        if(localName != null && !localName.isEmpty()) {
            element = localName;
            return;
        }

        element = qName;
    }

    @Override
    public void warning(SAXParseException exception) throws SAXException {
        String message = "Element named " +  element + ": " + exception.getMessage();
        LOG.warn(message);
    }

    @Override
    public void error(SAXParseException exception) throws SAXException {
        String message = "Element named " +  element + ": " + exception.getMessage();
        LOG.error(message);
		validationResult.addXmlError(message);
        errorOccurred = true;
    }

    @Override
    public void fatalError(SAXParseException exception) throws SAXException {
        String message = "Element named " +  element + ": " + exception.getMessage();
        LOG.error(message);
        errorOccurred = true;
		validationResult.addXmlError(message);
        throw new SAXException( message );
    }

    public boolean isErrorOccurred() {
        return errorOccurred;
    }

    public void setErrorOccurred(boolean errorOccurred) {
        this.errorOccurred = errorOccurred;
    }

    public String getElement() {
        return element;
    }

	public XmlValidationResult getValidationResult() {
		return validationResult;
	}

	public void setValidationResult(XmlValidationResult validationResult) {
		this.validationResult = validationResult;
	}
}
