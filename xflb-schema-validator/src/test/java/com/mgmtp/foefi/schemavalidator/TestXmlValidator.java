package com.mgmtp.foefi.schemavalidator;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Test;

class TestXmlValidator {

	@Test
	void validateTrue() throws IOException {
		Reader xsdReader = new InputStreamReader(
				readFromFile("case01/person.xsd"), StandardCharsets.UTF_8);
		Reader xmlReader = new InputStreamReader(
				readFromFile("case01/person.xml"), StandardCharsets.UTF_8);

		assertTrue(new XmlValidator().validate(xsdReader, xmlReader).isOK());
	}

	@Test
	void validateXflbTrue() throws IOException {
		Reader xmlReader = new InputStreamReader(
				readFromFile("xflb/xflb-sample.xml"), StandardCharsets.UTF_8);
		assertTrue(XmlValidatorFactory.getDefaultValidator().validateXflb(xmlReader).isOK());
	}

	@Test
	void validateFalse() throws IOException {
		Reader xsdReader = new InputStreamReader(
				readFromFile("case01/full-person.xsd"), StandardCharsets.UTF_8);
		Reader xmlReader = new InputStreamReader(
				readFromFile("case01/person.xml"), StandardCharsets.UTF_8);
		assertFalse(new XmlValidator().validate(xsdReader, xmlReader).isOK());
	}

	@Test
	void validateFeature() throws IOException {
		Reader xsdReader = new InputStreamReader(
				readFromFile("case01/full-person.xsd"), StandardCharsets.UTF_8);
		XmlValidationResult result = new XmlValidator().validate(xsdReader, new InputStreamReader(
				readFromFile("case01/person2.xml"), StandardCharsets.UTF_8));
		assertFalse(result.isOK());
		assertTrue(result.getXmlErrors().get(0).contains("Element named : DOCTYPE is disallowed"));
	}

	private InputStream readFromFile(String filepath) {
		return Thread
				.currentThread()
				.getContextClassLoader()
				.getResourceAsStream(filepath);
	}

}
