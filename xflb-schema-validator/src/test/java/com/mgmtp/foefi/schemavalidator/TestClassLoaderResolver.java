package com.mgmtp.foefi.schemavalidator;

import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TestClassLoaderResolver {

	@Test
	void getResourceAsStreamTest() throws IOException {
		InputStream is = new ClassLoaderResolver("case01")
				.getResourceAsStream("person.xsd");
		Assertions.assertNotNull(is);
		is.close();
	}
}