# XFLB Förderchecker 



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.opencode.de/OC000029002073/xflb-foerderchecker.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.opencode.de/OC000029002073/xflb-foerderchecker/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***


## Name
Foerderchecker

## Description
This project can be used to validate any XML data with respect to the XÖV data standard XFoerderleistungsbeschreibung (XSD Format). The developement language is Java.
The project also offers a general XML validator against any input XSD file.

## References
For more information about the standard and the 
applicated project, visit https://docs.fitko.de/fit-standards/xflb/ , https://www.stmd.bayern.de/themen/digitale-verwaltung/foerderfinder/ or https://www.xrepository.de/ . 

## Installation and Usage
For usage you can download the code of this project via the methods which opencode gitlab offers. After Intgrating the code into your IDE, you should directly be able to start the application. Java Doc is provided within the source code.

## Support
If you have questions or need help regarding this project you can contact us via mail: xflb@stmd.bayern.de

## Authors and acknowledgment
Jan Kubetschek,
Kostadin Dimov,
David Biam (StMD),
acknowledgment to the Förderfinder Project Team

## License
GNU Affero General Public License, Version 3 (AGPL-3.0)

